/*------------------------------------------------------
 * Variables
 * -----------------------------------------------------
 */

var snake;
var snakeLength;
var snakeSize;
var snakeDirection;

var food;

var context;
var screenWidth;
var screenHeight;

var gameState;
var mamba;
var indigo;
var startUp;
var playButton;
var gameOverMenu;
var restartButton;
var playHUD;
var scorboard;
var howTo;
var playHUM;
var audio;
var sound;
var noise;
var music;
var image;

/*------------------------------------------------------
 * Executing Game Code
 * -----------------------------------------------------
 */

gameInitialize();
snakeInitialize();
foodInitialize();

/*------------------------------------------------------
 * Game Functions
 * -----------------------------------------------------
 */

function gameInitialize() {
    var canvas = document.getElementById("game-screen");
    screenColor = "rgb(7, 17, 51)";
    context = canvas.getContext("2d");
    
    screenWidth = window.innerWidth - 43;
    screenHeight = window.innerHeight - 40;
    
    canvas.width = screenWidth;
    canvas.height = screenHeight;
    
    document.addEventListener("keydown", keyboardHandler);
    
    startUp = document.getElementById("start");
    centerMenuPosition(startUp);
    
    playButton = document.getElementById("playButton");
    playButton.addEventListener("click", gameNormal);
    
    playButton = document.getElementById("playButtons");
    playButton.addEventListener("click", gameSlow);
    
    playButton = document.getElementById("playButtonf");
    playButton.addEventListener("click", gameFast);
    
    gameOverMenu = document.getElementById("gameOver");
    centerMenuPosition(gameOverMenu);
    
    highScoreMenu = document.getElementById("highScore");
    
    restartButton = document.getElementById("restartButton");
    restartButton.addEventListener("click", gameRestart);
    
    menuButton = document.getElementById("menuButton");
    menuButton.addEventListener("click", gameReload);
    
    playHUD = document.getElementById("playHUD");
    scoreboard = document.getElementById("scoreboard");
    
    playHUM = document.getElementById("hi");
    howTo = document.getElementById("la");
    
    image = document.getElementById("source");
    
    music = new Audio('Kalimba.mp3');
    
    drawHowto();
    
    setState("START");
}

function gameLoop() {
    gameDraw();
    drawScoreboard();
    if(gameState == "PLAY") {
        snakeUpdate();
        snakeDraw();
        foodDraw();
    }
}

function gameDraw() {
    context.fillStyle = screenColor;
    context.fillRect (0, 0, screenWidth, screenHeight);
}

function gameRestart() {
    snakeInitialize();
    foodInitialize();
    music.play();
    hideMenu(gameOverMenu);
    setState("PLAY");
}

function gameNormal() {
    snakeInitialize();
    foodInitialize();
    hideMenu(startUp);
    setState("PLAY");
    indigo = "blue";
    music.play();
    setInterval(gameLoop, 1000/20);
}

function gameSlow() {
    snakeInitialize();
    foodInitialize();
    hideMenu(startUp);
    setState("PLAY");
    music.play();
    setInterval(gameLoop, 1000/7);
}

function gameFast() {
    snakeInitialize();
    foodInitialize();
    hideMenu(startUp);
    setState("PLAY");
    mamba = "black";
    music.play();
    setInterval(gameLoop, 1000/50);
}

function gameReload() {
    location.reload();
}

/*------------------------------------------------------------
 * Snake Functions
 * -----------------------------------------------------------
 */

function snakeInitialize() {
    snake = [];
    snakeLength = 1;
    snakeSize = 24;
    snakeDirection = "down";
    
    for(var index = snakeLength - 1; index >= 0; index--) {
        snake.push( {
            x: index,
            y: 0
        }); 
        
    }
    
}

function snakeDraw() {
    if(mamba == "black") {
        for(var index = 0; index < snake.length; index++){
            context.fillStyle = "black";
            context.strokeStyle = "#071134";
            context.strokeRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);        
            context.fillRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
            context.lineWidth = 2;
        }
    }
    else {
        for(var index = 0; index < snake.length; index++){
            context.strokeStyle = "#071134";
            context.strokeRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
            context.fillStyle = "indigo";
            context.fillRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
            context.lineWidth = 2;
        }
    }
    if(indigo == "blue") {
        for(var index = 0; index < snake.length; index++){
            context.strokeStyle = "#071134";
            context.strokeRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
            context.fillStyle = "chartreuse";
            context.fillRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
            context.lineWidth = 2;
        }
    }
}

function snakeUpdate() {
    var snakeHeadX = snake[0].x;
    var snakeHeadY = snake[0].y;
    
    if(snakeDirection == "down") {
        snakeHeadY++;
    }
    
    else if(snakeDirection == "right") {
        snakeHeadX++;
    }
    
    if(snakeDirection == "up") {
        snakeHeadY--;
    }
    
    else if(snakeDirection == "left") {
        snakeHeadX--;
    }
    
    checkFoodCollisions(snakeHeadX, snakeHeadY);
    checkWallCollisions(snakeHeadX, snakeHeadY);
    checkSnakeCollisions(snakeHeadX, snakeHeadY);
    
    var snakeTail = snake.pop();
    snakeTail.x = snakeHeadX;
    snakeTail.y = snakeHeadY;
    snake.unshift(snakeTail);
}

/*----------------------------------------------------------------
 * Food Functions
 * ---------------------------------------------------------------
 */

function foodInitialize(){
    food = {
        x: 0,
        y: 0
    };
    setFoodPosition();
}

function foodDraw() {
    context.drawImage(image, food.x * snakeSize, food.y * snakeSize, 30, snakeSize);
}

function setFoodPosition() {
    var randomX = Math.floor(Math.random() * (screenWidth));
    var randomY = Math.floor(Math.random() * (screenHeight));
    
    food.x = Math.floor(randomX / snakeSize);
    food.y = Math.floor(randomY / snakeSize);
}

/* --------------------------------------------------------------
 * Input Functions
 * --------------------------------------------------------------
 */

function keyboardHandler(event) {
    console.log(event);
    
    if(event.keyCode == "39" && snakeDirection != "left") {
        snakeDirection = "right";
    }
    
    else if(event.keyCode == "40" && snakeDirection != "up") {
        snakeDirection = "down";
    }
    
    if(event.keyCode == "37" && snakeDirection != "right") {
        snakeDirection = "left";
    }
    
    else if(event.keyCode == "38" && snakeDirection != "down") {
        snakeDirection = "up";
    }
}

/*---------------------------------------------------------------
 * Collision Handling
 * --------------------------------------------------------------
 */
function checkFoodCollisions(snakeHeadX, snakeHeadY) {
    if(snakeHeadX == food.x && snakeHeadY == food.y) {
        snake.push({
           x: 0,
           y: 0
        });
        snake.push({
           x: 0,
           y: 0
        });
        snake.push({
           x: 0,
           y: 0
        });
        snakeLength++;
        snakeLength++;
        snakeLength++;
        setFoodPosition();
        audio.play();
    }
    audio = new Audio('dog.mp3');
}

function checkWallCollisions(snakeHeadX, snakeHeadY) {
    if(snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0) {
        setState("GAME OVER");
        music.pause();
    }
    
    if(snakeHeadY * snakeSize >= screenHeight || snakeHeadY * snakeSize < 0) {
        setState("GAME OVER");
        music.pause();
    }
}

function checkSnakeCollisions(snakeHeadX, snakeHeadY) {
    for(var index = 1; index < snake.length; index++) {
        if(snakeHeadX == snake[index].x && snakeHeadY == snake[index].y) {
            setState("GAME OVER");
            music.pause();
            return;          
        }
    }
    
}

/*---------------------------------------------------------------
 * Game State Handling
 * --------------------------------------------------------------
 */

function setState(state) {
    gameState = state;
    showMenu(state);
}

/*---------------------------------------------------------------
 * Menu Functions
 * --------------------------------------------------------------
 */

function displayMenu(menu) {
    menu.style.visibility = "visible";
}

function hideMenu(menu) {
    menu.style.visibility = "hidden";
}

function showMenu(state) {
    if(state == "GAME OVER") {
        displayMenu(gameOverMenu);
        sound.play();
    }
    else if(state == "PLAY") {
        displayMenu(playHUD);
        
    }
    sound = new Audio('explosion.mp3');
}

function displayPlay(play) {
    play.style.visibility = "visible";
}

function hidePlay(play) {
    play.style.visibility = "hidden";
}

function showPlay(state) {
    if(state == "START") {
        displayMenu(startUp);
        displayMenu(playHUM);
    }
    else if(state == "PLAY") {
        displayMenu(playHUD);
    }
}

function centerMenuPosition(menu) {
    menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
    menu.style.left = (screenWidth / 2) - (menu.offsetWidth / 2) + "px";
}

function drawScoreboard() {
    scoreboard.innerHTML = "Length: " + snakeLength;
}

function drawHowto() {
    howTo.innerHTML = "DIRECTIONS";
}